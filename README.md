## Chat App

### Install
Run the following commands to install the app:
```
git clone https://github.com/ceoandri/chat-app.git
cd chat-app
npm install
```
#### Run Application
Run the following command to run the app:
```
npm start
```
View the app running in production at [http://localhost:3000](http://localhost:3000)

View the demo app at [https://chat-app001.herokuapp.com/](https://chat-app001.herokuapp.com/)