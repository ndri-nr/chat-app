// config.js
export default {
  bucket: {
    slug: '9c8f7380-35c0-11e8-b179-43a1cea9f019',
    type_slug: 'messages'
  },
  server: {
    host: process.env.APP_URL || 'http://localhost:3000'
  }
}